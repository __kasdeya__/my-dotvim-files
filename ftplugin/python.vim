setlocal colorcolumn=79
set textwidth=79

" Auto-wrap [t]ext and [c]omments
setlocal formatoptions+=tc

" Allows running and debugging
nmap <F5> :up\|!python %<CR>
nmap <F6> :up\|!python -m pdb %<CR>
nmap <F7> :up\|!python -i %<CR>

command -bar -nargs=0 -buffer Pdb
            \ norm Oimport pdb; pdb.set_trace() # TODO: remove debug code
